package utils;

import org.testng.annotations.Test;

import java.io.IOException;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class PropertyFileEditorTest {

    @Test
    public void performReadWriteDeleteEntry() throws IOException {
        PropertyFileEditor propertyFileEditor = new PropertyFileEditor("src/test/resources/test_data.properties");

        propertyFileEditor.writeValue("test.data", "test data");

        assertEquals(propertyFileEditor.readValue("test.data"), "test data");

        propertyFileEditor.removeEntry("test.data");

        assertNull(propertyFileEditor.readValue("test.data"));
    }
}