package unit;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class TheUnitTest {

    @Test
    public void testUnitTestIsReallyUnit() {
        String whoAmI = "I'm a unit test Sir!";

        assertEquals("I'm a unit test Sir!", whoAmI);
    }
}
