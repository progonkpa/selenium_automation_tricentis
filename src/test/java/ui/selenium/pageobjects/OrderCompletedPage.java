package ui.selenium.pageobjects;

import ui.selenium.base.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class OrderCompletedPage extends PageObject {

    @FindBy(xpath = "//strong[text() = \"Your order has been successfully processed!\"]")
    private WebElement orderSuccessfulMessage;

    public OrderCompletedPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isInitialized() {
        return orderSuccessfulMessage.isDisplayed();
    }
}
