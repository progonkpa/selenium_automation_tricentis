package ui.selenium.pageobjects;

import ui.selenium.base.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends PageObject {

    public static String URL = "http://demowebshop.tricentis.com/";

    @FindBy(xpath = "//div[@class=\"header-links\"]//a[@class=\"account\"]")
    private WebElement accountLinkHeader;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public String getHeaderAccountEmailText() {
        return accountLinkHeader.getText();
    }
}
