package ui.selenium.pageobjects;

import ui.selenium.base.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegisterPage extends PageObject {

    public static String URL = "http://demowebshop.tricentis.com/register";

    @FindBy(id = "gender-male")
    private WebElement maleRb;
    @FindBy(id = "gender-female")
    private WebElement femaleRb;
    @FindBy(id = "FirstName")
    private WebElement firstName;
    @FindBy(id = "LastName")
    private WebElement lastName;
    @FindBy(id = "Email")
    private WebElement email;
    @FindBy(id = "Password")
    private WebElement password;
    @FindBy(id = "ConfirmPassword")
    private WebElement confirmPassword;
    @FindBy(id = "register-button")
    private WebElement registerBtn;

    public RegisterPage(WebDriver driver) {
        super(driver);
    }

    public void clickMaleRb() {
        maleRb.click();
    }

    public void clickFemaleRb() {
        femaleRb.click();
    }

    public void enterName(String firstName, String lastName) {
        this.firstName.clear();
        this.lastName.clear();
        this.firstName.sendKeys(firstName);
        this.lastName.sendKeys(lastName);
    }

    public void enterEmail(String email) {
        this.email.clear();
        this.email.sendKeys(email);
    }

    public void enterPassword(String password) {
        this.password.clear();
        this.password.sendKeys(password);
    }

    public void enterConfirmPassword(String confirmPassword) {
        this.confirmPassword.clear();
        this.confirmPassword.sendKeys(confirmPassword);
    }

    public String registrationErrorMessage() {
        WebElement registrationErrorMessage = driver.findElement(By.xpath("//div[@class=\"validation-summary-errors\"]/*/li[1]"));
        return registrationErrorMessage.getText();
    }

    public RegistrationCompletedPage clickRegisterBtn() {
        registerBtn.click();
        return new RegistrationCompletedPage(driver);
    }
}

