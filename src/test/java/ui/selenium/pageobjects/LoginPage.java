package ui.selenium.pageobjects;

import ui.selenium.base.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class LoginPage extends PageObject {

    public static String URL = "http://demowebshop.tricentis.com/login";

    @FindBy(id = "Email")
    private WebElement email;
    @FindBy(id = "Password")
    private WebElement password;
    @FindBy(xpath = "//input[@type=\"submit\" and contains(@class, \"login-button\")]")
    private WebElement loginButton;
    @FindBy(css = "input.button-1.checkout-as-guest-button")
    private WebElement checkoutAsGuestButton;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void enterEmail(String email) {
        this.email.sendKeys(email);
    }

    public void enterPassword(String password) {
        this.password.sendKeys(password);
    }

    public HomePage clickLogin() {
        this.loginButton.click();
        return new HomePage(driver);
    }

    public String getValidationSummaryErrorsMessage() {
        return driver.findElement(By.xpath("//div[@class=\"validation-summary-errors\"]/span")).getText();
    }

    public List<String> getValidationSummaryErrors() {
        WebElement errorsUl = driver.findElement(By.xpath("//div[@class=\"validation-summary-errors\"]/ul"));
        List<WebElement> errorLis = errorsUl.findElements(By.tagName("li"));
        List<String> errors = new ArrayList<>();
        errorLis.stream().map(WebElement::getText).forEach(errors::add);
        return errors;
    }

    public OnePageCheckout clickCheckoutAsGuest() {
        this.checkoutAsGuestButton.click();
        return new OnePageCheckout(driver);
    }
}
