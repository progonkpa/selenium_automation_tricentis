package ui.selenium.pageobjects;

import ui.selenium.base.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class BookCategoryPage extends PageObject {

    public static String URL = "http://demowebshop.tricentis.com/books";

    @FindBy(xpath = "//div[@class=\"product-grid\"]/div[@class=\"item-box\"]")
    private List<WebElement> productItems;

    public BookCategoryPage(WebDriver driver) {
        super(driver);
    }

    public void addBookToCart(int index) {
        WebElement productItem = productItems.get(index);
        WebElement addToCartButton = productItem.findElement(By.xpath("//input[@value=\"Add to cart\"]"));
        addToCartButton.click();
    }
}
