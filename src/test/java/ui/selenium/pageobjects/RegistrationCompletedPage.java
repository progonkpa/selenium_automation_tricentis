package ui.selenium.pageobjects;

import ui.selenium.base.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegistrationCompletedPage extends PageObject {

    @FindBy(className = "result")
    private WebElement result;

    public RegistrationCompletedPage(WebDriver driver) {
        super(driver);
    }

    public boolean isInitialized() {
        return result.isDisplayed();
    }
}
