package ui.selenium.pageobjects;

import ui.selenium.base.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class CartPage extends PageObject {

    @FindBy(xpath = "//div[@class=\"page-title\"]/h1")
    private WebElement pageTitle;

    @FindBy(xpath = "//select[@id='StateProvinceId']")
    private WebElement zipPostalCodeTextField;
    @FindBy(css = "input#termsofservice")
    private WebElement tosCheckbox;
    @FindBy(css = "div.checkout-buttons button[type=\"submit\"]")
    private WebElement checkoutSubmitButton;

    public CartPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isInitialized() {
        return pageTitle.isDisplayed();
    }

    public void enterCountryDropdown(String country) {
        WebElement countryDropdown = waitUntilElementVisible(By.xpath("//select[@id='CountryId']"));
        waitUntilElementVisible(By.xpath("//select[@id='CountryId']/option[text() = \"" + country + "\"]"));
        Select dropdown = new Select(countryDropdown);
        dropdown.selectByVisibleText(country);
    }

    public void enterStateProvinceDropdown(String state) {
        WebElement stateProvinceDropdown = waitUntilElementVisible(By.xpath("//select[@id='StateProvinceId']"));
        waitUntilElementVisible(By.xpath("//*[@id=\"StateProvinceId\"]/option"));
        Select dropdown = new Select(stateProvinceDropdown);
        dropdown.selectByVisibleText(state);
    }

    public void enterZipPostalCodeTextField(String zipPostalCode) {
        this.zipPostalCodeTextField.sendKeys(zipPostalCode);
    }

    public void clickTosCheckbox() {
        tosCheckbox.click();
    }

    public LoginPage clickCheckoutButton() {
        checkoutSubmitButton.click();
        return new LoginPage(driver);
    }
}
