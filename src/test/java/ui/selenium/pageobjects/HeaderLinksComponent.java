package ui.selenium.pageobjects;

import ui.selenium.base.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HeaderLinksComponent extends PageObject {

    @FindBy(css = "div.header-links a[href=\"/register\"]")
    private WebElement registerLink;
    @FindBy(css = "div.header-links a[href=\"/login\"]")
    private WebElement loginLink;
    @FindBy(css = "div.header-links a[href=\"/cart\"]")
    private WebElement cartLink;
    @FindBy(css = "div.header-links a[href=\"/wishlist\"]")
    private WebElement wishlistLink;

    public HeaderLinksComponent(WebDriver driver) {
        super(driver);
    }

    public RegisterPage clickRegisterLink() {
        registerLink.click();
        return new RegisterPage(driver);
    }

    public LoginPage clickLoginLink() {
        loginLink.click();
        return new LoginPage(driver);
    }

    public CartPage clickCartLink() {
        cartLink.click();
        return new CartPage(driver);
    }

    public WishlistPage clickWishlistLink() {
        return new WishlistPage(driver);
    }
}
