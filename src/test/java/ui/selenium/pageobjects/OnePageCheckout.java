package ui.selenium.pageobjects;

import ui.selenium.base.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class OnePageCheckout extends PageObject {

    @FindBy(xpath = "//h1[text() = \"Checkout\"]")
    private WebElement pageTitle;

    @FindBy(css = "#BillingNewAddress_FirstName")
    private WebElement firstNameTextfield;
    @FindBy(css = "#BillingNewAddress_LastName")
    private WebElement lastNameTextfield;
    @FindBy(css = "#BillingNewAddress_Email")
    private WebElement emailTextfield;
    @FindBy(css = "#BillingNewAddress_CountryId")
    private WebElement countrySelect;
    @FindBy(css = "#BillingNewAddress_City")
    private WebElement cityTextfield;
    @FindBy(css = "#BillingNewAddress_Address1")
    private WebElement addressTextfield;
    @FindBy(css = "#BillingNewAddress_ZipPostalCode")
    private WebElement zipTextfield;
    @FindBy(css = "#BillingNewAddress_PhoneNumber")
    private WebElement phoneTextfield;
    @FindBy(css = "li#opc-billing input[type=button]")
    private WebElement billingContinueButton;

    private String shippingContinueCssSelector = "li#opc-shipping input[type=button]";
    private String shippingMethodContinueCssSelector = "li#opc-shipping_method input[type=button]";
    private String paymentMethodXpath = "//*[@id=\"payment-method-buttons-container\"]/input";
    private String paymentInfoContinueCssSelector = "li#opc-payment_info input[type=button]";
    private String confirmOrderContinueCssSelector = "li#opc-confirm_order input[type=button]";

    public OnePageCheckout(WebDriver driver) {
        super(driver);
    }

    @Override
    public boolean isInitialized() {
        return pageTitle.isDisplayed();
    }

    public void clickBillingContinueButton() {
        billingContinueButton.click();
    }

    public void clickShippingContinueButton() {
        clickContinue(shippingContinueCssSelector);
    }

    public void clickShippingMethodContinueButton() {
        clickContinue(shippingMethodContinueCssSelector);
    }

    public void clickPaymentMethodContinueButton() {
        waitUntilElementVisible(By.xpath(paymentMethodXpath)).click();
    }

    public void clickPaymentInfoContinueButton() {
        clickContinue(paymentInfoContinueCssSelector);
    }

    public OrderCompletedPage clickConfirmOrderContinueButton() {
        clickContinue(confirmOrderContinueCssSelector);
        return new OrderCompletedPage(driver);
    }

    private void clickContinue(String cssSelector) {
        WebElement webElement = driver.findElement(By.cssSelector(cssSelector));
        webElement.click();
    }

    public void enterFirstNameTextfield(String firstName) {
        this.firstNameTextfield.sendKeys(firstName);
    }

    public void enterLastNameTextfield(String lastName) {
        this.lastNameTextfield.sendKeys(lastName);
    }

    public void enterEmailTextfield(String email) {
        this.emailTextfield.sendKeys(email);
    }

    public void selectCountry(String country) {
        Select dropdown = new Select(countrySelect);
        dropdown.selectByVisibleText(country);
    }

    public void enterCityTextfield(String city) {
        this.cityTextfield.sendKeys(city);
    }

    public void enterAddressTextfield(String address) {
        this.addressTextfield.sendKeys(address);
    }

    public void enterZipTextfield(String zip) {
        this.zipTextfield.sendKeys(zip);
    }

    public void enterPhoneTextfield(String phone) {
        this.phoneTextfield.sendKeys(phone);
    }
}
