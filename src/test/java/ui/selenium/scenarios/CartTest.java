package ui.selenium.scenarios;

import ui.selenium.base.SeleniumTest;
import ui.selenium.pageobjects.BookCategoryPage;
import ui.selenium.pageobjects.CartPage;
import ui.selenium.pageobjects.HeaderLinksComponent;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class CartTest extends SeleniumTest {

    @Test
    public void addFirstBook() {
        driver.get(BookCategoryPage.URL);
        BookCategoryPage bookCategoryPage = new BookCategoryPage(driver);

        bookCategoryPage.addBookToCart(0);
        HeaderLinksComponent headerLinks = new HeaderLinksComponent(driver);
        CartPage cartPage = headerLinks.clickCartLink();

        assertTrue(cartPage.isInitialized());
    }
}
