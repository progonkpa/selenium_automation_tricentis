package ui.selenium.scenarios;

import ui.selenium.base.SeleniumTest;
import ui.selenium.pageobjects.HomePage;
import ui.selenium.pageobjects.LoginPage;
import org.testng.annotations.Test;

import static ui.selenium.scenarios.RegisterTest.*;
import static org.testng.Assert.assertEquals;

public class LoginTest extends SeleniumTest {

    @Test
    public void loginWithValidUser() {
        driver.get(LoginPage.URL);
        LoginPage loginPage = new LoginPage(driver);
        String email = FIRST_NAME + "." + LAST_NAME + "@gmail.com";

        loginPage.enterEmail(email);
        loginPage.enterPassword(PASSWORD);
        HomePage homePage = loginPage.clickLogin();

        assertEquals(homePage.getHeaderAccountEmailText(), email);
    }

    @Test
    public void loginWithInvalidUser() {
        driver.get(LoginPage.URL);
        LoginPage loginPage = new LoginPage(driver);
        String email = "randomJosIsRandom1923847120394871320984@gmail.com";

        loginPage.enterEmail(email);
        loginPage.enterPassword(PASSWORD);
        HomePage homePage = loginPage.clickLogin();

        assertEquals("Login was unsuccessful. Please correct the errors and try again.", loginPage.getValidationSummaryErrorsMessage());
        assertEquals("No customer account found", loginPage.getValidationSummaryErrors().get(0));
    }
}
