package ui.selenium.scenarios;

import ui.selenium.base.SeleniumTest;
import ui.selenium.pageobjects.RegisterPage;
import ui.selenium.pageobjects.RegistrationCompletedPage;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class RegisterTest extends SeleniumTest {

    static final String FIRST_NAME = "Jos";
    static final String LAST_NAME = "Bos";
    static final String PASSWORD = "uncrackablePassword";

    @Test
    public void registerNewUniqueMaleUserSucceeds() {
        driver.get(RegisterPage.URL);

        String firstName = FIRST_NAME + getTimeBasedUniqueNumber();

        RegisterPage registerPage = new RegisterPage(driver);
        registerPage.clickMaleRb();
        registerPage.enterName(firstName, LAST_NAME);
        registerPage.enterEmail(firstName + "." + LAST_NAME + "@gmail.com");
        registerPage.enterPassword(PASSWORD);
        registerPage.enterConfirmPassword(PASSWORD);
        RegistrationCompletedPage registrationCompletedPage = registerPage.clickRegisterBtn();

        assertTrue(registrationCompletedPage.isInitialized());
    }

    @Test
    public void registerNewUniquefemaleUserSucceeds() {
        driver.get(RegisterPage.URL);

        String firstName = FIRST_NAME + getTimeBasedUniqueNumber();

        RegisterPage registerPage = new RegisterPage(driver);
        registerPage.clickFemaleRb();
        registerPage.enterName(firstName, LAST_NAME);
        registerPage.enterEmail(firstName + "." + LAST_NAME + "@gmail.com");
        registerPage.enterPassword(PASSWORD);
        registerPage.enterConfirmPassword(PASSWORD);
        RegistrationCompletedPage registrationCompletedPage = registerPage.clickRegisterBtn();

        assertTrue(registrationCompletedPage.isInitialized());
    }

    @Test
    public void registerExistingUserFails() {
        driver.get(RegisterPage.URL);

        RegisterPage registerPage = new RegisterPage(driver);
        registerPage.clickMaleRb();
        registerPage.enterName(FIRST_NAME, LAST_NAME);
        registerPage.enterEmail(FIRST_NAME + "." + LAST_NAME + "@gmail.com");
        registerPage.enterPassword(PASSWORD);
        registerPage.enterConfirmPassword(PASSWORD);
        registerPage.clickRegisterBtn();

        assertEquals("The specified email already exists", registerPage.registrationErrorMessage());
    }

    private String getTimeBasedUniqueNumber() {
        DateTimeFormatter timeStampPattern = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        return timeStampPattern.format(LocalDateTime.now(ZoneId.of("Europe/Brussels")));
    }
}
