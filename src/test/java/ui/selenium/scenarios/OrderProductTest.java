package ui.selenium.scenarios;

import org.testng.annotations.Test;
import ui.selenium.base.RetryAnalyzer;
import ui.selenium.base.SeleniumTest;
import ui.selenium.pageobjects.*;

import static org.testng.Assert.assertTrue;

public class OrderProductTest extends SeleniumTest {

    @Test(retryAnalyzer = RetryAnalyzer.class)
    public void orderABookAnonymously() {
        driver.get(BookCategoryPage.URL);
        BookCategoryPage bookCategoryPage = new BookCategoryPage(driver);

        bookCategoryPage.addBookToCart(0);
        HeaderLinksComponent headerLinks = new HeaderLinksComponent(driver);
        CartPage cartPage = headerLinks.clickCartLink();

        cartPage.enterCountryDropdown("Belgium");
        cartPage.enterStateProvinceDropdown("Other (Non US)");
        cartPage.enterZipPostalCodeTextField("1000");
        cartPage.clickTosCheckbox();
        LoginPage loginPage = cartPage.clickCheckoutButton();

        OnePageCheckout onePageCheckout = loginPage.clickCheckoutAsGuest();

        fillNewAddress(onePageCheckout);
        onePageCheckout.clickBillingContinueButton();
        onePageCheckout.clickShippingContinueButton();
        onePageCheckout.clickShippingMethodContinueButton();
        onePageCheckout.clickPaymentMethodContinueButton();
        onePageCheckout.clickPaymentInfoContinueButton();
        OrderCompletedPage orderCompletedPage = onePageCheckout.clickConfirmOrderContinueButton();

        assertTrue(orderCompletedPage.isInitialized());
    }

    private void fillNewAddress(OnePageCheckout onePageCheckout) {
        onePageCheckout.enterFirstNameTextfield("Jos");
        onePageCheckout.enterLastNameTextfield("Bos");
        onePageCheckout.enterEmailTextfield("jos@bos.be");
        onePageCheckout.selectCountry("Belgium");
        onePageCheckout.enterCityTextfield("Brussels");
        onePageCheckout.enterAddressTextfield("Troonplein 1");
        onePageCheckout.enterZipTextfield("1000");
        onePageCheckout.enterPhoneTextfield("0485123456");
    }
}
