package ui.selenium.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class SeleniumTest {

    protected static WebDriver driver;

    @BeforeClass
    public static void beforeClass() {
        instantiateChromeDriver();
    }

    @AfterMethod
    public void afterMethod() {
        driver.manage().deleteAllCookies();
    }

    @AfterClass
    public static void afterClass() {
        driver.close();
    }

    private static void instantiateChromeDriver() {
        System.setProperty(
                "webdriver.chrome.driver",
                // Azure hosted agent environment variable ChromeWebDriver.
                new File(System.getenv("ChromeWebDriver"), "chromedriver.exe").getPath()
        );
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }
}
