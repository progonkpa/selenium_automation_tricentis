package utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class PropertyFileEditor {

    private Properties properties;
    private String relativePathPropertyFile;

    public PropertyFileEditor(String relativePathPropertyFile) throws IOException {
        this.relativePathPropertyFile = relativePathPropertyFile;
        properties = new Properties();
        FileInputStream fileInputStream = new FileInputStream(relativePathPropertyFile);
        properties.load(fileInputStream);
    }

    public String readValue(String key) {
        return properties.getProperty(key);
    }

    public void writeValue(String key, String value) {
        properties.setProperty(key, value);
        store(properties);
    }

    public void removeEntry(String key) {
        properties.remove(key);
        store(properties);
    }

    private void store(Properties properties) {
        try (OutputStream fileOutputStream = new FileOutputStream(relativePathPropertyFile)) {
            properties.store(fileOutputStream, "");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
